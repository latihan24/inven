@extends('frontend.main_master')
@section('main')

@section('title')
Home | Dwina Ahsani Website
@endsection

<!-- banner-area -->
@include('frontend.home_all.home_slide')
<!-- banner-area-end -->

<!-- about-area -->
@include('frontend.home_all.home_about')
<!-- about-area-end -->

<!-- services-area -->
<section class="services">
    <div class="container">
        <div class="services__title__wrap">
            <div class="row align-items-center justify-content-between">
                <div class="col-xl-5 col-lg-6 col-md-8">
                    <div class="section__title">
                        <span class="sub-title">02 - my Collection</span>
                        <h2 class="title">Collection Sport Car</h2>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6 col-md-4">
                    <div class="services__arrow"></div>
                </div>
            </div>
        </div>
        <div class="row gx-0 services__active">
            <div class="col-xl-3">
                <div class="services__item">
                    <div class="services__thumb">
                        <img height="240" src="{{ asset('frontend/assets/img/po.jpg') }}" alt="">
                    </div>
                    <div class="services__content">
                        <div class="services__icon">
                            <img class="light" src="{{ asset('frontend/assets/img/icons/services_light_icon01.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/services_icon01.png') }}" alt="">
                        </div>
                        <h3 class="title"><a href="services-details.html">Porsche 911</a></h3>
                        <p> Diluncurkan pada tahun 1963, menjadi ikon dalam dunia mobil sport. Meskipun mengalami evolusi desain, Porsche 911 mempertahankan bentuk khasnya dengan mesin di bagian belakang</p>
                        <ul class="services__list">
                            <li>Desain yang klasik </li>
                            <li>Performa Luar Biasa</li>
                            <li>Varian Model</li>
                        </ul>
                        <a href="services-details.html" class="btn border-btn">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="services__item">
                    <div class="services__thumb">
                        <img height="240" src="{{ asset('frontend/assets/img/ch.jpg') }}" alt="">
                    </div>
                    <div class="services__content">
                        <div class="services__icon">
                            <img class="light" src="{{ asset('frontend/assets/img/icons/services_light_icon02.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/services_icon02.png') }}" alt="">
                        </div>
                        <h3 class="title"><a href="services-details.html">Chevrolet Corvette</a></h3>
                        <p>Corvette pertama kali diperkenalkan pada tahun 1953 dan salah satu mobil sport Amerika yang paling terkenal. Mobil ini telah mengalami beberapa generasi dengan perubahan desain dan performa.
                        </p>
                        <ul class="services__list">
                            <li>Desain Menawan</li>
                            <li>Performa dan Harga</li>
                            <li>Teknologi Terbaru</li>
                        </ul>
                        <a href="services-details.html" class="btn border-btn">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="services__item">
                    <div class="services__thumb">
                        <img height="240" src="{{ asset('frontend/assets/img/fo.jpg') }}" alt="">
                    </div>
                    <div class="services__content">
                        <div class="services__icon">
                            <img class="light" src="{{ asset('frontend/assets/img/icons/services_light_icon03.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/services_icon03.png') }}" alt="">
                        </div>
                        <h3 class="title"><a href="services-details.html">Ford Mustang</a></h3>
                        <p>Ford Mustang diluncurkan pada tahun 1964 dan dikenal sebagai salah satu mobil pony car pertama. Mustang telah menjadi simbol budaya Amerika dengan performa dan gaya yang unik.</p>
                        <ul class="services__list">
                            <li>Warisan Budaya</li>
                            <li>Pilihan Mesin</li>
                            <li>Desain Ikonik</li>
                        </ul>
                        <a href="services-details.html" class="btn border-btn">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="services__item">
                    <div class="services__thumb">
                        <img height="240" src="{{ asset('frontend/assets/img/maz.jpg') }}" alt="">
                    </div>
                    <div class="services__content">
                        <div class="services__icon">
                            <img class="light"  src="{{ asset('frontend/assets/img/icons/services_light_icon04.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/services_icon04.png') }}" alt="">
                        </div>
                        <h3 class="title"><a href="services-details.html">Mazda MX-5 Miata</a></h3>
                        <p>MX-5 Miata pertama kali diperkenalkan pada tahun 1989 dan menjadi roadster yang populer dan terjangkau. Miata menawarkan pengalaman mengemudi klasik dengan desain ringkas.</p>
                        <ul class="services__list">
                            <li>Handling Responsif</li>
                            <li>Desain Klasik</li>
                            <li>Terjangkau</li>
                        </ul>
                        <a href="services-details.html" class="btn border-btn">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3">
                <div class="services__item">
                    <div class="services__thumb">
                        <img height="240" src="{{ asset('frontend/assets/img/lamb.jpg') }}" alt="">
                    </div>
                    <div class="services__content">
                        <div class="services__icon">
                            <img class="light" src="{{ asset('frontend/assets/img/icons/services_light_icon02.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/services_icon02.png') }}" alt="">
                        </div>
                        <h3 class="title"><a href="services-details.html">Lamborghini Gallardo</a></h3>
                        <p> Lamborghini Gallardo adalah model sukses yang diproduksi antara tahun 2003 hingga 2013. Sebuah mobil paling laku dalam 7 tahun produksi. Ini adalah mobil sport entry-level dari Lamborghini.</p>
                        <ul class="services__list">
                            <li>Desain Eksotis</li>
                            <li>Percepatan Cepat</li>
                            <li>Aksesibilitas Lamborghini</li>
                        </ul>
                        <a href="services-details.html" class="btn border-btn">Read more</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- services-area-end -->

<!-- work-process-area -->
<section class="work__process">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <div class="section__title text-center">
                    <span class="sub-title">03 - Rent Process</span>
                    <h2 class="title">How To Rent a Car</h2>
                </div>
            </div>
        </div>
        <div class="row work__process__wrap">
            <div class="col">
                <div class="work__process__item">
                    <span class="work__process_step">Step - 01</span>
                    <div class="work__process__icon">
                        <img class="light" src="{{ asset('frontend/assets/img/icons/wp_light_icon01.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/wp_icon01.png') }}" alt="">
                    </div>
                    <div class="work__process__content">
                        <h4 class="title">Contact Us</h4>
                        <p>Silakan hubungi kami untuk layanan rental mobil yang nyaman dan handal.</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="work__process__item">
                    <span class="work__process_step">Step - 02</span>
                    <div class="work__process__icon">
                        <img class="light" src="{{ asset('frontend/assets/img/icons/wp_light_icon02.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/wp_icon02.png') }}" alt="">
                    </div>
                    <div class="work__process__content">
                        <h4 class="title">Ask</h4>
                        <p>Apakah ada pilihan mobil yang tersedia untuk disewa?</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="work__process__item">
                    <span class="work__process_step">Step - 03</span>
                    <div class="work__process__icon">
                        <img class="light" src="{{ asset('frontend/assets/img/icons/wp_light_icon03.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/wp_icon03.png') }}" alt="">
                    </div>
                    <div class="work__process__content">
                        <h4 class="title">Book</h4>
                        <p> Memilih mobil yang tersedia untuk disewa.</p>
                    </div>
                </div>
            </div>
            <div class="col">
                <div class="work__process__item">
                    <span class="work__process_step">Step - 04</span>
                    <div class="work__process__icon">
                        <img class="light" src="{{ asset('frontend/assets/img/icons/wp_light_icon04.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/wp_icon04.png') }}" alt="">
                    </div>
                    <div class="work__process__content">
                        <h4 class="title">Rent</h4>
                        <p>Silahkan menggunakan mobil dari layanan rental kami.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- work-process-area-end -->

<!-- portfolio-area -->
@include('frontend.home_all.portfolio')
<!-- portfolio-area-end -->

<!-- partner-area -->
<section class="partner">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <ul class="partner__logo__wrap">
                    <li>
                        <img class="light" src="{{ asset('frontend/assets/img/l_bmw.jpg') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/partner_01.png') }}" alt="">
                    </li>
                    <li>
                        <img class="light" src="{{ asset('frontend/assets/img/l_forlo.jpg') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/partner_02.png') }}" alt="">
                    </li>
                    <li>
                        <img class="light" src="{{ asset('frontend/assets/img/l_hon.jpg') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/partner_03.png') }}" alt="">
                    </li>
                    <li>
                        <img class="light" src="{{ asset('frontend/assets/img/l_toy.jpg') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/partner_04.png') }}" alt="">
                    </li>
                    <li>
                        <img class="light" src="{{ asset('frontend/assets/img/l_merc.png') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/partner_05.png') }}" alt="">
                    </li>
                    <li>
                        <img class="light" src="{{ asset('frontend/assets/img/l_ch.jpg') }}" alt="">
    <img class="dark" src="{{ asset('frontend/assets/img/icons/partner_06.png') }}" alt="">
                    </li>
                </ul>
            </div>
            <div class="col-lg-6">
                <div class="partner__content">
                    <div class="section__title">
                        <span class="sub-title">05 - partners</span>
                        <h2 class="title">I proud to have collaborated with some awesome companies</h2>
                    </div>
                    <p>"Proudly, the car rental is delighted to have successfully collaborated with a renowned car manufacturer."</p>
                    <a href="{{ route('contact.me') }}" class="btn">Start a conversation</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- partner-area-end -->

<!-- testimonial-area 
<section class="testimonial">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-lg-6 order-0 order-lg-2">
                <ul class="testimonial__avatar__img">
                    <li><img src="{{ asset('frontend/assets/img/images/testi_img01.png') }}" alt=""></li>
    <li><img src="{{ asset('frontend/assets/img/images/testi_img02.png') }}" alt=""></li>
    <li><img src="{{ asset('frontend/assets/img/images/testi_img03.png') }}" alt=""></li>
    <li><img src="{{ asset('frontend/assets/img/images/testi_img04.png') }}" alt=""></li>
    <li><img src="{{ asset('frontend/assets/img/images/testi_img05.png') }}" alt=""></li>
    <li><img src="{{ asset('frontend/assets/img/images/testi_img06.png') }}" alt=""></li>
    <li><img src="{{ asset('frontend/assets/img/images/testi_img07.png') }}" alt=""></li>
                </ul>
            </div>
            <div class="col-xl-5 col-lg-6">
                <div class="testimonial__wrap">
                    <div class="section__title">
                        <span class="sub-title">06 - Client Feedback</span>
                        <h2 class="title">Happy clients feedback</h2>
                    </div>
                    <div class="testimonial__active">
                        <div class="testimonial__item">
                            <div class="testimonial__icon">
                                <i class="fas fa-quote-left"></i>
                            </div>
                            <div class="testimonial__content">
                                <p>We are motivated by the satisfaction of our clients. Put your trust in us &share in
                                    our H.Spond Asset Management is made up of a team of expert, committed and
                                    experienced people with a passion for financial markets. Our goal is to achieve
                                    continuous.</p>
                                <div class="testimonial__avatar">
                                    <span>Rasalina De Wiliamson</span>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial__item">
                            <div class="testimonial__icon">
                                <i class="fas fa-quote-left"></i>
                            </div>
                            <div class="testimonial__content">
                                <p>We are motivated by the satisfaction of our clients. Put your trust in us &share in
                                    our H.Spond Asset Management is made up of a team of expert, committed and
                                    experienced people with a passion for financial markets. Our goal is to achieve
                                    continuous.</p>
                                <div class="testimonial__avatar">
                                    <span>Rasalina De Wiliamson</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="testimonial__arrow"></div>
                </div>
            </div>
        </div>
    </div>
</section>
 testimonial-area-end -->

 
<!-- blog-area -->
@include('frontend.home_all.home_blog')
<!-- blog-area-end -->

<!-- contact-area 
<section class="homeContact">
    <div class="container">
        <div class="homeContact__wrap">
            <div class="row">
                <div class="col-lg-6">
                    <div class="section__title">
                        <span class="sub-title">07 - Say hello</span>
                        <h2 class="title">Any questions? Feel free <br> to contact</h2>
                    </div>
                    <div class="homeContact__content">
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                            suffered alteration in some form</p>
                        <h2 class="mail"><a href="mailto:Info@webmail.com">Info@webmail.com</a></h2>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="homeContact__form">
                        <form action="#">
                            <input type="text" placeholder="Enter name*">
                            <input type="email" placeholder="Enter mail*">
                            <input type="number" placeholder="Enter number*">
                            <textarea name="message" placeholder="Enter Massage*"></textarea>
                            <button type="submit">Send Message</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 contact-area-end -->

@endsection